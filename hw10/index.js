const tabBtn = document.querySelectorAll(".tabs-title");

tabBtn.forEach((tabBtn) => {
  tabBtn.addEventListener("click", () => {
    const tab = document.querySelector(`.tab-content[data-tab="${tabBtn.dataset.btn}"]`);

    document.querySelector(".tab-content.open").classList.remove("open");
    document.querySelector(".tabs-title.active").classList.remove("active");

    tab.classList.add("open");
    tabBtn.classList.add("active");

  });
});
