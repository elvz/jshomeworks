const switchIcon = document.querySelector(".theme-switch-icon");
const form = document.querySelector(".form-wrapper");
let icon = window.localStorage.setItem("icon", switchIcon.className);

switchIcon.addEventListener("click", () => {
  let theme;
  
console.log(switchIcon.className);
  if (switchIcon.classList.contains("fa-moon")) {
    switchIcon.classList.replace("fa-moon", "fa-sun");
    theme = form.style.background = "#000000";
    localStorage.setItem("theme", theme);
    localStorage.setItem("icon", switchIcon.className);
  } else if (switchIcon.classList.contains("fa-sun")) {
    switchIcon.classList.replace("fa-sun", "fa-moon");
    theme = form.style.background = "#ffffff";
    localStorage.setItem("theme", theme);
    localStorage.setItem("icon", switchIcon.className);
  }
});

form.style.background = localStorage.getItem("theme");
switchIcon.className = localStorage.getItem("icon");
