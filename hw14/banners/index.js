const images = document.querySelectorAll(".image-to-show");
const slide = document.querySelector(".slide");
const buttons = document.querySelectorAll(".buttons");

let imgSrcArray = [];
let i = 0;

images.forEach((img) => {
  imgSrcArray.push(img.src);
});

const slideImage = () => {
  slide.src = imgSrcArray[i];

  if (i < images.length - 1) {
    i++;
  } else {
    i = 0;
  }
};

const startSlide = setInterval(slideImage, 3000);

const stopSlide = () => {
  clearInterval(startSlide);
};

buttons.forEach((btn) => {
  btn.addEventListener("click", (e) => {
    if (e.target.innerHTML == "Возобновить") {
      setInterval(slideImage, 3000);
    } else if (e.target.innerHTML == "Прекратить") {
      stopSlide();
    }
  });
});