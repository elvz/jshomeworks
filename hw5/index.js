const createNewUser = () => {
  let firstName = prompt("Enter your first name");
  let lastName = prompt("Enter your last name");

  const newUser = {
    firstName,
    lastName,
    set fName(fName) {
      this.firstName = fName;
    },
    set lName(lName) {
      this.lastName = lName;
    },
    getLogin() {
      let firstChar = this.firstName.charAt(0).toLowerCase();
      let lowerCasedLastName = this.lastName.toLowerCase();
      alert(`Welcome! Your login is ${firstChar + lowerCasedLastName}`);
    },
  };

  fName = newUser.firstName;
  lName = newUser.lastName;

  newUser.getLogin();

  console.log(typeof newUser);

  Object.defineProperties(newUser, {
    firstName: {
      value: this.firstName,
      writable: false,
    },
    lastName: {
      value: this.lastName,
      writable: false,
    },
  });
};

createNewUser();
