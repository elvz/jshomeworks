let firstNumber = +prompt("Enter first number");
let secondNumber = +prompt("Enter second number");
// let mathSign = prompt("Enter sign: + - * or /");

while (isNaN(firstNumber) || firstNumber == "") {
  firstNumber = +prompt("Enter first number");
}

while (isNaN(secondNumber) || secondNumber == "") {
  secondNumber = +prompt("Enter second number");
}

const getSign = () => {
  let userChoice;
  do {
    userChoice = prompt("Enter sign: + - * or /", userChoice)
  } while (userChoice !== "+" && userChoice !== "-" && userChoice !== "*" && userChoice !== "/");
  return userChoice;
}


const calculate = (num1, num2) => {
  const userChoice = getSign();
  let result;
  switch (userChoice) {
    case "+":
      result = Number(num1) + Number(num2);
      break;
    case "-":
      result = Number(num1) - Number(num2);
      break;
    case "*":
      result = Number(num1) * Number(num2);
      break;
    case "/":
      result = Number(num1) / Number(num2);
      break;
    default:
      alert("error");
      break;
  }
  return result;
};

alert(`Result is ${calculate(firstNumber, secondNumber)}`);
