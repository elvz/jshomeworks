1. Описати своїми словами навіщо потрібні функції у програмуванні.

Функції у програмуванні відіграють велику роль, коли потрібно перевикористовувати одну дію декілька разів.

2. Описати своїми словами, навіщо у функцію передавати аргумент.

Передані у функцію аргументи копіюються в локальні змінні, з подальшим викорастанням в самій функції.

3. Що таке оператор return та як він працює всередині функції?

Оператор return повертає значення функції і зупиняє її виконання. Можна використовувати return кілька разів у різних областях видимості.