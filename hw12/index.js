const buttons = document.querySelectorAll(".btn");

document.addEventListener("keypress", (e) => {
  buttons.forEach((btn) => {
    btn.classList.remove("btn-pressed");
    console.log(e.key);
    e.key.toUpperCase() == btn.innerHTML || e.code == btn.innerHTML
      ? btn.classList.add("btn-pressed")
      : null;
  });
});