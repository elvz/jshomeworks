//forEach

// let array = ["hello", "world", 23, "23", null];
// let dataType = "string";

// const filterBy = ((arr, type) => {
//   let result = [];
//   arr.forEach(el => {
//     if (typeof el !== type) {
//       result.push(el);
//     }
//   })
//   console.log(result);
// })

// filterBy(array, dataType);

//filter

let array = ["hello", "world", 23, "23", null];
let dataType = "string";

const filterBy = (arr, type) => {
  let result = [];
  result = arr.filter((el) => {
    return typeof el !== type;
  });
  console.log(result);
};

filterBy(array, dataType);

//wrong 

// let array = ["hello", "world", 23, "23", null];
// let dataType = "string";

// const filterBy = (arr, type) => {
//   let i = arr.length;
//   while (i--) {
//     if (typeof arr[i] === type) {
//       arr.splice(i, 1);
//     }
//   }
//   console.log(arr);;
// };

// filterBy(array, dataType);

