let name = prompt("Enter your name");
let age = +prompt("Enter your age");

while (name == "") {
  name = prompt("Enter your name");
}

while (isNaN(age) || age == '') {
  age = +prompt("Enter your age");
}

if (age < 18) {
  alert("You are not allowed to visit this website");
} else if (age >= 18 && age < 21) {
  let accept = confirm("Are you sure you want to continue?");
  if (!accept) {
    alert("You are not allowed to visit this website");
  } else {
    alert(`Welcome ${name}`);
  }
} else {
  alert(`Welcome ${name}`);
}
