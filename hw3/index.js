//first part

let userInput = +prompt("Enter your number");

while (isNaN(userInput) || userInput == "") {
  userInput = +prompt("Enter your number");
}

if (userInput < 5) {
  console.log("No such a numbers");
} else {
  for (let i = 0; i <= userInput; i++) {
    if (i % 5 == 0) {
      console.log(i);
    }
  }
}

//second part

let m = +prompt("Enter first number");
let n = +prompt("Enter second number");

while (!Number.isInteger(m) || m == "") {
  m = +prompt("Error! Enter first number");
}

while (!Number.isInteger(n) || n == "") {
  n = +prompt("Error! Enter second number");
}

if (m < n) {
  for (i = m; i <= n; i++) {
    console.log(i);
  }
} else {
  console.log("No such a number");
}
