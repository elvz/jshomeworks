const arrayToDom = (array, node) => {
  node = document.querySelector(node);
  let ul = document.createElement("ul");
  let subUl = document.createElement("ul");
  node.appendChild(ul);
  ul.appendChild(subUl);

  let subArr;

  const arr = array.map((el) => {
    let template = `<li>${el}</li>`;

    if (Array.isArray(el)) {
      subArr = el;
    }

    if (Array.isArray(subArr)) {
      subArr = subArr;
    }

    return template;
  });

  const createHtmlFromArray = (arr) => `${arr.map((el) =>Array.isArray(el) ? createHtmlFromArray(el) : `${el}`).join('')}`;


  ul.insertAdjacentHTML("beforeend", arr.join(""));
  subUl.insertAdjacentHTML("beforeend", sub.join(""));
};

arrayToDom([[1, 2], 1, "hello", 4, true], "#app");
